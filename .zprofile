export GST_VAAPI_ALL_DRIVERS=1
export MOZ_ENABLE_WAYLAND=1
source /etc/profile
export PATH="$PATH:/home/paul/.local/share/JetBrains/Toolbox/scripts:/home/paul/.local/bin"
export ANDROID_SDK_ROOT=/home/paul/Android/Sdk
export _JAVA_AWT_WM_NONREPARENTING=1
#source /etc/zprofile
