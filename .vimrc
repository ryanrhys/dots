if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

set nocompatible
set number

au BufNewFile,BufRead /*.rasi setf css

set modeline
set tabstop=4
set shiftwidth=4
set autoindent
set expandtab
set wrap
set clipboard=unnamedplus

syntax enable

filetype plugin indent on
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

call plug#begin('~/.vim/plugged')
Plug 'jdonaldson/vaxe'
call plug#end()
