" Automatically Install vim-plug if not currently installed
if empty(glob(stdpath('data') . '/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif
" Vim-Plug Plugs/Plugins
call plug#begin(stdpath('data') . '/plugged')
Plug 'itchyny/lightline.vim'
call plug#end()

" ColorScheme Setup
" colorscheme pywal
" highlight Normal guibg=black
set background=light

"colorscheme onedark
"let g:onedark_termcolors = 16
"let g:lightline = {
"      \    'colorscheme': 'onedark',
"      \}

"let g:onedark_color_overrides = {
"      \ "black": {"gui": "#000000", "cterm": "235", "cterm16": "0" },
"      \ "red": {"gui": "#E27D60", "cterm": "204", "cterm16": "1" },
"      \ "green": {"gui": "#872DFF", "cterm": "114", "cterm16": "2" },
"      \ "yellow": {"gui": "#E8A87C", "cterm": "180", "cterm16": "3" },
"      \ "blue": {"gui": "#41B3A3", "cterm": "39", "cterm16": "4" },
"      \ "purple": {"gui": "#BF85DC", "cterm": "170", "cterm16": "5" },
"      \ "cyan": {"gui": "#00A6B2", "cterm": "38", "cterm16": "6" },
"      \ "white": {"gui": "#BFBFBF", "cterm": "145", "cterm16": "7" },
"      \ "menu_grey": {"gui": "#666666", "cterm": "237", "cterm16": "8" },
"      \ "cursor_grey": {"gui": "#666666", "cterm": "236", "cterm16": "8" },
"      \ "dark_red": {"gui": "#E50000", "cterm": "196", "cterm16": "9" },
"      \ "dark_yellow": {"gui": "#E5E500", "cterm": "173", "cterm16": "11" },
"      \ "comment_grey": {"gui": "#E5E5E5", "cterm": "59", "cterm16": "15" },
"      \ "gutter_fg_grey": {"gui": "#E5E5E5", "cterm": "238", "cterm16": "15" },
"      \ "visual_grey": {"gui": "#E5E5E5", "cterm": "237", "cterm16": "15" },
"      \ "special_grey": {"gui": "#E5E5E5", "cterm": "238", "cterm16": "15" },
"      \ "vertsplit": {"gui": "#E5E5E5", "cterm": "59", "cterm16": "15" },
"      \}
"
"au ColorScheme * hi Normal ctermbg=none guibg=none
"au ColorScheme myspecialcolors hi Normal ctermbg=red guibg=red

" General
au BufNewFile,BufRead /*.rasi setf css

set nocompatible
set number
set modeline
set tabstop=8
set shiftwidth=4
set autoindent
set expandtab
set wrap
set clipboard=unnamedplus
set laststatus=2
set noshowmode

let g:clipboard = {
          \   'name': 'unnamed',
          \   'copy': {
          \      '+': 'xclip -sel clip -i',
          \      '*': 'xclip -sel clip -i',
          \    },
          \   'paste': {
          \      '+': 'xclip -sel clip -o',
          \      '*': 'xclip -sel clip -o',
          \   },
          \   'cache_enabled': 1,
          \ }

" Save for wayland
" let g:clipboard = {
"           \   'name': 'unnamed',
"           \   'copy': {
"           \      '+': 'wl-copy',
"           \      '*': 'wl-copy',
"           \    },
"           \   'paste': {
"           \      '+': 'wl-paste',
"           \      '*': 'wl-paste',
"           \   },
"           \   'cache_enabled': 1,
"           \ }


syntax on

filetype plugin indent on
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

