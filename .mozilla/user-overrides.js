/* override recipe: enable session restore ***/
user_pref("browser.startup.page", 3); // 0102

user_pref("keyword.enabled", true); // 0801
user_pref("media.eme.enabled", true); // 2022

user_pref("privacy.clearOnShutdown.history", false); // 2803

user_pref("privacy.resistFingerprinting.letterboxing", false); // 4504

user_pref("privacy.clearOnShutdown.history", false); // 2800
user_pref("privacy.clearOnShutdown.sessions", false);
user_pref("privacy.clearOnShutdown.downloads", false);
user_pref("privacy.clearOnShutdown.cache", false);
user_pref("privacy.clearOnShutdown.offlineApps", false);
user_pref("privacy.clearOnShutdown.cookies", false);
user_pref("network.cookie.lifetimePolicy", 0);

user_pref("ui.systemUsesDarkTheme", 1); // [FF67+] [HIDDEN PREF]
user_pref("browser.display.background_color", "#1C1B22");

user_pref("network.http.referer.XOriginPolicy", 0);

