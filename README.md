# dots

```
git clone --bare https://gitlab.com/ryanrhys/dots.git $HOME/.dotfiles

alias dots='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

// Create lowercase user dir folders
mkdir {desktop,documents,downloads,music,pictures,public,templates,videos}

dots checkout
```
